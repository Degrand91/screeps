var roleBuilder = {

    /** @param {Creep} creep **/
    run: function(creep) {

        var target = creep.pos.findClosestByRange(FIND_DROPPED_ENERGY);

        if (creep.memory.working && creep.carry.energy == 0) creep.memory.working = false;
        if (!creep.memory.working && creep.carry.energy == creep.carryCapacity)  creep.memory.working = true;

        if(creep.memory.building) {
            var targets = creep.room.find(FIND_CONSTRUCTION_SITES);
            var repairTarget = creep.room.find(FIND_STRUCTURES, {
                    filter: object => object.hits < object.hitsMax
        });

            if(targets.length) {
                creep.say('B: building');
                if(creep.build(targets[0]) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(targets[0]);
                }
            } else if(repairTarget.length > 0) {
                creep.say('B: repair');
                repairTarget.sort((a,b) => a.hits - b.hits);
                if(creep.repair(repairTarget[0]) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(repairTarget[0]);
                }
            }
        } else {
            if(target) {
                if(creep.pickup(target) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target);
                }
            }
        }
    }
};

module.exports = roleBuilder;