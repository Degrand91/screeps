/**
 * Created by stefano on 27/01/17.
 */
var roleUpdater = {

    run: function(creep) {
        var target = creep.pos.findClosestByRange(FIND_DROPPED_ENERGY);
        if (creep.memory.working && creep.carry.energy == 0) creep.memory.working = false;
        if (!creep.memory.working && creep.carry.energy == creep.carryCapacity)  creep.memory.working = true;

        if(creep.memory.working) {
            if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller);
            }
        } else {
            if(target) {
                if(creep.pickup(target) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target);
                }
            }
        }
    }
};

module.exports = roleUpdater;