var roleMiner = {
    run: function(creep) {
        var mineId = creep.memory.mineId;
        var mine = Game.getObjectById(mineId);
        if (creep.carry.energy == creep.carryCapacity) {
            creep.drop("energy", creep.carry.energy);
        } else {
            if (creep.harvest(mine) == ERR_NOT_IN_RANGE) {
                creep.moveTo(mine);
            } else {
                creep.say('H: ' + creep.carry.energy);
            }
        }
    }
};


module.exports = roleMiner;