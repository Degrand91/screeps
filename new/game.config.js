var config = {
    alliance : ["helton"],
    creeps : {
        "miner":{
            base:[WORK,MOVE,CARRY],
            powerUp:[WORK]
        },
        "deliver":{
            base:[WORK,MOVE,CARRY],
            powerUp:[CARRY,MOVE,MOVE]
        },
        "updater":{
            base:[WORK,MOVE,CARRY],
            powerUp:[CARRY,MOVE,MOVE]
        },
        "builder":{
            base:[WORK,MOVE,CARRY],
            powerUp:[MOVE,MOVE,WORK]
        }
    },
    rooms :{
        "W18N89": {
            spawnName: 'Alfa',
            mines: {
                "5873bcd911e3e4361b4d8482":{ min :2, count: 0 },
                "5873bcd911e3e4361b4d8484":{ min :2, count: 0 }
            },
            creationOrder: ["miner","deliver","updater","builder"],
            registry:{
                "miner" :   {min : 3, live:0, level:1},
                "deliver" : {min : 1, live:0, level:0},
                "updater" : {min : 1, live:0, level:0},
                "builder" : {min : 0, live:0, level:0}
            }
        }
    }
};

module.exports = config;