var roleDeliver = {

    /** @param {Creep} creep **/
    run: function(creep) {

        var target = creep.pos.findClosestByRange(FIND_DROPPED_ENERGY);
        if (creep.memory.working && creep.carry.energy == 0) creep.memory.working = false;
        if (!creep.memory.working && creep.carry.energy == creep.carryCapacity)  creep.memory.working = true;

        if(creep.memory.working) {
            var structures = creep.room.find(FIND_STRUCTURES, {
                filter: function(structure){
                    return (structure.structureType == STRUCTURE_EXTENSION
                        || structure.structureType == STRUCTURE_SPAWN
                        || structure.structureType == STRUCTURE_TOWER
                        || structure.structureType == STRUCTURE_CONTAINER)
                        && structure.energy < structure.energyCapacity;
                }
            });
            if(structures.length > 0) {
                if(creep.transfer(structures[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(structures[0]);
                }
            } else {
                // nothing to do
            }
        } else {
            if(target) {
                if(creep.pickup(target) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target);
                }
            }
        }
    }
};


module.exports = roleDeliver;