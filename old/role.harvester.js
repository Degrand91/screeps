var roleHarvester = {

    /** @param {Creep} creep **/
    run: function(creep) {
        var mineId = creep.memory.mineId;
        var mine = Game.getObjectById(mineId) || creep.room.find(FIND_SOURCES);
        
        if (creep.memory.working && creep.carry.energy == 0) creep.memory.working = false;
        if (!creep.memory.working && creep.carry.energy == creep.carryCapacity)  creep.memory.working = true;

        if(creep.memory.working) {
            var structures = creep.room.find(FIND_STRUCTURES, {
                filter: function(structure){
                            return (structure.structureType == STRUCTURE_EXTENSION 
                                    || structure.structureType == STRUCTURE_SPAWN
                                    || structure.structureType == STRUCTURE_TOWER
                                    || structure.structureType == STRUCTURE_CONTAINER)
                                    && structure.energy < structure.energyCapacity;
                }
            });
            
            if(structures.length > 0) {
                if(creep.transfer(structures[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(structures[0]);
                    creep.say('H: ♻');
                }
            } else {
                creep.say('H: ⛾');
                creep.moveTo(15,23);
            }
        } else {
            if(creep.harvest(mine) == ERR_NOT_IN_RANGE) {
                creep.moveTo(mine);
                creep.say('H: ☭ ');
            }

        }
    }
};
    

module.exports = roleHarvester;