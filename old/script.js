var creepsInv : {
    harvester: { min : 2, body :[WORK,CARRY,CARRY,MOVE], count:0},
    upgrader: { min : 4, body :[WORK,CARRY,CARRY,MOVE], count:0 },
    builder: { min : 3, body :[WORK,CARRY,CARRY,MOVE], count:0 },
    warrior: { min : 5, body :[ATTACK,ATTACK,MOVE,MOVE], count:0 },
    
}

for(var name in Game.creeps) {
        var creep = Game.creeps[name];
        var role = creep.memory.role;
        creepsInv[role].count++;
}

for(var role in creepsInv){
    var min = creepsInv[role].min;
    var count = creepsInv[role].count;
    console.log(role+": "+ count +"/"+min)
    if(Game.spawns.Spawn1.energy > 200 && count < min ){
        Game.notify(role+": "+ count +"/"+min);
        Game.spawns.Spawn1.createCreep(creepsInv[role].body,null,{role:role})
    }
}