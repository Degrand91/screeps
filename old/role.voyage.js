var roleVoyage = {

    /** @param {Creep} creep **/
    attack: function(creep,target) {
        if(target) {
            creep.say("⚔ "+target.hits,false)
            if(creep.attack(target) == ERR_NOT_IN_RANGE) {
                creep.moveTo(target);
            } 
        }
    },
    explore: function (creep,inventory){
        var dest = creep.memory.roomDest;
        var creepRoom = creep.pos.roomName;
        var alliance = inventory.alliance;
        var hostiles = Game.rooms[creepRoom].find(FIND_HOSTILE_CREEPS);
        var target = null;
        if(hostiles.length > 0) {
            var closestHostile = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS) || creep.pos.findClosestByRange(FIND_HOSTILE_STRUCTURES);;
            var owner = closestHostile.owner.username;
            var isAlliance = (alliance.indexOf(owner) >= 0);
            if (closestHostile && !isAlliance) {
                target = closestHostile;
            } else {
                for (var h = 0; h < hostiles.length; h++) {
                    owner = hostiles[h].owner.username;
                    isAlliance = (alliance.indexOf(owner) >= 0);
                    if (!isAlliance && !target) {
                        target = hostiles[h];
                    }
                }
            }
        }
        //target = Game.getObjectById("5897116070f6eaa00580b5a7");
        if(creep.room.name != dest){
            creep.say("V: "+dest,false)
            creep.moveTo(new RoomPosition(18,46, dest));
        } else if(target) {
            this.attack(creep,target);
        } else if(creep.room.controller && !creep.room.controller.my) {
            if(creep.claimController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller);
            } else {
                creep.moveTo(creep.room.controller);
            }
        } else {
            creep.moveTo(creep.room.controller);
            creep.say("V:⛾")
        } 
    }
};

module.exports = roleVoyage;

