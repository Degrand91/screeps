var roleGuard = {

    /** @param {Creep} creep **/
    defend: function(creep,inventory) {
        var creepRoom = creep.pos.roomName;
        var alliance = inventory.alliance;
        var hostiles = Game.rooms[creepRoom].find(FIND_HOSTILE_CREEPS);
        var target = null;
        if(hostiles.length > 0) {
            var closestHostile = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
            var owner = closestHostile.owner.username;
            var isAlliance = (alliance.indexOf(owner) >= 0);
            if(closestHostile && !isAlliance) {
                target = closestHostile;
            } else {
                for(var h = 0; h< hostiles.length; h++){
                    owner = hostiles[h].owner.username;
                    isAlliance = (alliance.indexOf(owner) >= 0);
                    if(!isAlliance ){
                        target = hostiles[h];
                    }
                }
            }
            if(!target) return;
            if(creep.attack(target) == ERR_NOT_IN_RANGE) {
                creep.say('G: move');
                creep.moveTo(target);
            } else {
                creep.say('G: ⚔');
                creep.attack(target);
            }

        } else {
            creep.say("G : rest");
            
        }
    }      
}

module.exports = roleGuard;

