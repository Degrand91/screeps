var roleWarrior = {

    /** @param {Creep} creep **/
    attack: function(creep,target) {
        if(target) {
            creep.say("Att! "+target.hits,false)
            if(creep.attack(target) == ERR_NOT_IN_RANGE) {
                creep.moveTo(target);
            } 
        }
    },
    explore: function (creep,inventory){
        var dest = creep.memory.roomDest;
        var creepRoom = creep.pos.roomName;
        var alliance = inventory.alliance;
        var hostiles = Game.rooms[creepRoom].find(FIND_HOSTILE_CREEPS);
        var target = null;
        if(hostiles.length > 0) {
            var closestHostile = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS) || creep.pos.findClosestByRange(FIND_HOSTILE_STRUCTURES);;
            var owner = closestHostile.owner.username;
            var isAlliance = (alliance.indexOf(owner) >= 0);
            if (closestHostile && !isAlliance) {
                target = closestHostile;
            } else {
                for (var h = 0; h < hostiles.length; h++) {
                    owner = hostiles[h].owner.username;
                    isAlliance = (alliance.indexOf(owner) >= 0);
                    if (!isAlliance && !target) {
                        target = hostiles[h];
                    }
                }
            }
        }
        //target = Game.getObjectById("58943e05b8db850f888c5c62");
        //target = Game.getObjectById("58988cb278cff2452b41c80a");
        if(creep.room.name != dest){
            creep.say("W: "+dest,false)
            creep.moveTo(new RoomPosition(18,46, dest));
        } else if(target) {
            this.attack(creep,target);
        } else if(creep.room.name == dest){
            creep.moveTo(3,45);
        } else {
            creep.say("W: rest")
        } 
    }
}

module.exports = roleWarrior;

