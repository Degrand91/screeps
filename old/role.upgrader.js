/**
 * Created by stefano on 27/01/17.
 */
var roleUpgrader = {

    /** @param {Creep} creep **/
    run: function(creep) {
        var mineId = creep.memory.mineId ;
        var mine = Game.getObjectById(mineId) || creep.room.find(FIND_SOURCES);

        if (creep.memory.working && creep.carry.energy == 0) creep.memory.working = false;
        if (!creep.memory.working && creep.carry.energy == creep.carryCapacity)  creep.memory.working = true;

        if(creep.memory.working) {
            if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller);
            }
        } else {
            if (creep.harvest(mine) == ERR_NOT_IN_RANGE) {
                creep.moveTo(mine);
            }
        }
    }
};

module.exports = roleUpgrader;