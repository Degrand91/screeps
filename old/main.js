var utils = require('game.utils');
var config = require('game.config');
var roleMiner = require('role.miner');
var roleDeliver = require('role.deliver');
var roleUpdater = require('role.updater');
var roleBuilder = require('role.builder');


//module.exports.loop = function () {

    utils.freeMemory();
    
    for(var roomId in config.rooms){
        var room = config.rooms[roomId];
        //utils.defendRoom(room,config.alliance,1);
        utils.countCreeps(config,roomId);
        utils.countMiners(config,roomId);
        utils.updateCreepsRegistry(roomId,config,true);
        //gameUtils.reloadMemory(inventory)
        var mines = config.rooms[roomId].mines;
    }
    var minerCount = 0;
    for(var name in Game.creeps) {
        var creep = Game.creeps[name];
        var roomId = creep.memory.room;

        if(creep.pos.roomName != creep.memory.room){
            creep.memory.room = creep.pos.roomName;
            //console.log(creep.pos.roomName);
            //creep.moveTo(new RoomPosition(4,23, creep.memory.room));
        }
    
        if(creep.memory.role == 'miner') {
            if(!creep.memory.mineId){
                var mines = config.rooms[creep.pos.roomName].mines
                var mineIds = Object.keys(mines);
                creep.memory.mineId = mineIds[(minerCount%2)];
                
            }
            roleMiner.run(creep);
            minerCount++;
        }
        if(creep.memory.role == 'updater') {
            roleUpdater.run(creep);
        }
        if(creep.memory.role == 'deliver') {
            roleDeliver.run(creep);
        }
        if(creep.memory.role == 'builder') {
            roleDeliver.run(creep);
        }
        
    }
    
    
    for(var roomId in config.rooms){
        //console.log(JSON.stringify(mines));
        utils.resetRegistry(config,roomId);
    }
