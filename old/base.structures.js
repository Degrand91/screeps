var structure = {
    
    createRoad : function (creep){
        var role = creep.memory.role;
        if(["upgrader"].indexOf(role) >=0){
            Game.spawns['Alfa'].room.createConstructionSite( creep.pos.x, creep.pos.y, STRUCTURE_ROAD );        
        }
    },
    removeConstructionSite:function(creep){
        var targets = creep.room.find(FIND_CONSTRUCTION_SITES);
        targets.forEach(function(target){
            target.remove()
        })
    },
    createALLExtensions : function(){
        var positions = [   
        [ 9,12],[ 9,13],[ 9,14],[ 9,15],[ 9,16],
        [10,12],[10,13],[10,14],[10,15],[10,16],
        [14,12],[14,13],[14,14],[14,15],[14,16],
        [12,12],[12,13],[12,14],[12,15],[12,16]
        ];
        for(var r = 0; r < positions.length; r++){
            Game.spawns['Alfa'].room.createConstructionSite( positions[r][0], positions[r][1], STRUCTURE_EXTENSION );    
        }
    }
};

module.exports = structure;

