var gameUtils = {
    freeMemory: function () {
        for(var i in Memory.creeps) {
            if (!Game.creeps[i]) {
                delete Memory.creeps[i];
            }
        }
    },
    countCreeps : function (config,roomId){
        var room = config.rooms[roomId];
        for (var name in Game.creeps){
            var creep = Game.creeps[name];
            var creepRoom = creep.memory.room;
            var creepRole = creep.memory.role;
            if(!creepRole) { console.log(name+ " has no role!"); continue;}
            room.registry[creepRole].live++;
        }
    },
    countMiners :function(config,roomId){
        var room = config.rooms[roomId];
        for (var name in Game.creeps){
            var creep = Game.creeps[name];
            var mineId = creep.memory.mineId;
            var creepRole = creep.memory.role;
            if(creepRole == "miner" && mineId) room.mines[mineId].count ++;
        }
        return room.mines;
    },
    updateCreepsRegistry : function (roomId,config,DEBUG){
        var room = config.rooms[roomId];
        if(!room) return null;
        var creationOrder = room.creationOrder;
        for(var c = 0; c < creationOrder.length; c++){
            var creepRole = creationOrder[c];
            if(!creepRole) continue;
            var creepMin = room.registry[creepRole].min;
            var creepsLevel = room.registry[creepRole].level;
            var creepsCount = room.registry[creepRole].live;
            var creepInfo = config.creeps[creepRole];
            var creepMemory = { role: creepRole, room:roomId };
            for(var key in creepInfo.memory){
                creepMemory[key] = creepInfo.memory[key];
            }
            if(creepsCount < creepMin && DEBUG) console.log("["+roomId+"] "+creepRole +": "+ creepsCount +"/"+creepMin)
            if(Game.spawns[room.spawnName].energy && creepsCount < creepMin ){
                var bodyParts = this.createBodyParts(creepsLevel,creepInfo.base,creepInfo.powerUp);
                Game.spawns[room.spawnName].createCreep(bodyParts,null,creepMemory)
            }
        }
    },
    resetRegistry:function(config,roomId){
        var room = config.rooms[roomId];
        var mines = config.rooms[roomId].mines;
        if(room){
            for(var role in room.registry){
                room.registry[role].live = 0;
            }
        }
        if(mines){
            for (var mineId in mines) {
                mines[mineId].count = 0;
            }
        }
    },
    createBodyParts: function(level, base, powerUp){
        var bodyParts = base;
        for (var i = 0; i < level; i++){
            powerUp.forEach(function(part){
                bodyParts.push(part);
            })
        }
        return bodyParts;
    },
    setMineId : function(roomId,creep,config){
        var mines = config.rooms[roomId].mines;
        var minesOrder = [];
        for (var mineId in mines) {
            var count = mines[mineId].count;
            if(minesOrder.length){
                var oldMineId =  minesOrder[0];
                var oldCount = mines[oldMineId].count;
                if(oldCount > count){
                     minesOrder.unshift(mineId);
                }
            } else {
                minesOrder.unshift(mineId);
                
            }
        }
        mines[minesOrder[0]].count++;
        return minesOrder[0];
    },
    removeAllConstructionSite : function(creepName){
        var creep = Game.creeps[creepName];
        var targets = creep.room.find(FIND_CONSTRUCTION_SITES);
        targets.forEach(function(target){
            target.remove();
        })
    },
    defendRoom : function (room,alliance,level){
        var roomId = room.roomId;
        var towers = room.structures.towers;
        var hostiles = Game.rooms[roomId].find(FIND_HOSTILE_CREEPS);
        if(!level) level = 1;
        if(hostiles.length > 0) {
            if(level > 1 && Game.spawns[room.spawnName].hits < Game.spawns[room.spawnName].hitsMax) Game.rooms[roomId].controller.activateSafeMode();
            var owner = hostiles[0].owner.username;
            var isAlliance = (alliance.indexOf(owner) >= 0);
            Game.notify('User ${Owner} spotted');
            if(!isAlliance) {
                if(Game.spawns[room.spawnName].hits < Game.spawns[room.spawnName].hitsMax) Game.rooms[roomId].controller.activateSafeMode();
                for(var t = 0; t < towers.length; t++){
                    var towerId = towers[t].id;
                    var tower = Game.getObjectById(towerId);
                    if(tower) {
                        var closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
                        owner = hostiles[0].owner.username;
                        isAlliance = (alliance.indexOf(owner) >= 0);
                        if(closestHostile && !isAlliance) {
                            tower.attack(closestHostile);
                        } else {
                            for(var h = 0; h< hostiles.length; h++){
                                owner = hostiles[h].owner.username;
                                isAlliance = (alliance.indexOf(owner) >= 0);
                                if(!isAlliance && tower.attack(hostiles[h])) tower.attack(hostiles[h]);
                            }
                        }
                    }

                }

            }
        }
    },
    reloadMemory : function (inventory){
        for(var name in Game.creeps) {
            var creep = Game.creeps[name];
            var role = creep.memory.role;
            if(inventory.rooms[creep.pos.roomName]) creep.memory  = inventory.rooms[creep.pos.roomName].creeps[role].memory;
        }
    }
};

module.exports = gameUtils;