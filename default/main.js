var utils = require('game.utils');
var config = require('game.config');
var roleMiner = require('role.miner');
var roleDeliver = require('role.deliver');
var roleUpdater = require('role.updater');
var roleBuilder = require('role.builder');
var roleGuard = require('role.guard');
var roleWarrior = require('role.warrior');


//module.exports.loop = function () {

    utils.freeMemory();
    
    for(var roomId in config.rooms){
        var room = config.rooms[roomId];
        utils.defendRoom(roomId,config,1);
        utils.countCreeps(config,roomId);
        utils.countMiners(config,roomId);
        utils.updateCreepsRegistry(roomId,config,true);
        //console.log(JSON.stringify(config.rooms[roomId].registry));
    }
    var minerCount = 0;
    for(var name in Game.creeps) {
        var creep = Game.creeps[name];
        var roomId = creep.memory.room;

       
        if(creep.pos.roomName != creep.memory.room){
            //creep.memory.room = creep.pos.roomName;
            //console.log(creep.pos.roomName);
            creep.moveTo(new RoomPosition(14,3, creep.memory.room));
        }else {
            if(creep.memory.role == "claimer"){
                creep.moveTo(new RoomPosition(3,19, "W13N88"));
                if(creep.claimController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(creep.room.controller);
                }
                //creep.moveTo(7,31);
            }
            if(creep.memory.role == "warrior"){
                //creep.moveTo(new RoomPosition(3,19, "W13N89"));
                creep.moveTo(7,31);
                roleWarrior.attack(creep);
                
            }
            if(creep.memory.role == 'miner') {
                roleMiner.run(creep);
                minerCount++;
            }
            if(creep.memory.role == 'updater') {
                roleUpdater.run(creep);
            }
            if(creep.memory.role == 'deliver') {
                roleDeliver.run(creep);
            }
            if(creep.memory.role == 'builder') {
                roleBuilder.run(creep);
            }
            if(creep.memory.role == 'guard') {
                roleGuard.guard(creep,config);
            }
            
            if(creep.pos.roomName != creep.memory.room){
                //creep.memory.room = creep.pos.roomName;
                //console.log(creep.pos.roomName);
                creep.moveTo(new RoomPosition(4,23, creep.memory.room));
            }
        }
        
    }
    
    
    for(var roomId in config.rooms){
        //console.log(JSON.stringify(mines));
        utils.resetRegistry(config,roomId);
    }
    
    
    