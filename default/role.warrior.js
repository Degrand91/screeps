var roleWarrior = {

    attack: function (creep) {
        var target = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS) || creep.pos.findClosestByRange(FIND_HOSTILE_STRUCTURES);
        //var target = Game.getObjectById("589df97dd25357e8253c7847");
        if(target) {
            if(creep.attack(target) == ERR_NOT_IN_RANGE) {
                creep.say("att! "+target.hits);
                creep.moveTo(target);
            } else{
                
            }
        }
    }
}

module.exports = roleWarrior;

