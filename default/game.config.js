var config = {
    alliance : ["helton"],
    creeps : {
        "miner":{
            base:[WORK,MOVE,CARRY,CARRY],
            powerUp:[WORK]
        },
        "deliver":{
            base:[WORK,MOVE,MOVE,CARRY],
            powerUp:[CARRY,MOVE,MOVE]
        },
        "updater":{
            base:[WORK,MOVE,MOVE,CARRY],
            powerUp:[CARRY,MOVE,MOVE]
        },
        "builder":{
            base:[WORK,MOVE,MOVE,CARRY],
            powerUp:[MOVE,MOVE,WORK]
        },
        "guard":{
            base:[RANGED_ATTACK,RANGED_ATTACK,MOVE,MOVE],
            powerUp:[RANGED_ATTACK]
        },
        "warrior":{
            base:[ATTACK,MOVE],
            powerUp:[ATTACK,ATTACK]
        },
        "claimer":{
            base:[WORK,CARRY,WORK,MOVE],
            powerUp:[MOVE]
        }
    },
    rooms :{
        "W14N89": {
            spawnName: 'Alfa',
            mines: {
                "5873bce711e3e4361b4d85c1":{ min:1, count: 0 },
                "5873bce711e3e4361b4d85c2":{ min:1, count: 0 }
            },
            creationOrder: ["miner","deliver","updater","builder","guard","warrior","claimer"],
            registry:{
                "miner" :   {min : 2, live:0, level:3},
                "deliver" : {min : 3, live:0, level:0},
                "updater" : {min : 5, live:0, level:1},
                "builder" : {min : 2, live:0, level:1},
                "guard" : {min : 1, live:0, level:0},
                "tester" : {min : 0, live:0, level:0},
                "warrior" : {min : 0, live:0, level:1},
                "claimer" : {min : 0, live:0, level:0},
            },
            structures:{
                towers:["58a187485b78387c292fe95c"]
            }
        },              // second ROOM
        "W13N88": {
            spawnName: 'Alfa',
            mines: {
                "5873bcea11e3e4361b4d8629":{ min:1, count: 0 }
            },
            creationOrder: ["miner","deliver","updater","builder"],
            registry:{
                "miner" :   {min : 1, live:0, level:3},
                "deliver" : {min : 0, live:0, level:0},
                "updater" : {min : 1, live:0, level:0},
                "builder" : {min : 1, live:0, level:0},
                "guard" : {min : 0, live:0, level:0},
                "tester" : {min : 0, live:0, level:0},
                "warrior" : {min : 0, live:0, level:0},
                "claimer" : {min : 0, live:0, level:0},
            },
            structures:{
                towers:["58a187485b78387c292fe95c"]
            }
        }
    }
};

module.exports = config;