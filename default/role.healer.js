var roleHealer = {
    heal: function (me){
        var target = me.pos.findClosestByRange(FIND_MY_CREEPS, {
            filter: function(object) {
                return object.hits < object.hitsMax;
            }
        });
        if(target) {
            if(me.heal(target) == ERR_NOT_IN_RANGE) {
                me.moveTo(target);
            }
        } else {
            this.followWarriors(me)
        }

    },
    followWarriors: function(me){
        var targetName = me.memory.target;
        var target = Game.creeps[targetName]
        if(target){
            me.moveTo(target);
        } else {
            for(var name in Game.creeps){
                var creep = Game.creeps[name];
                var role = creep.memory.role;
                if(role == "warrior") {
                    me.memory.target = name;
                }
            }
        }
        /*
        var creeps = Game.creeps;
        if (0){
            me.moveTo(creep);
        } else {
            for(var name in creeps){
                var creep = creep[name];
                var role = creep.memory.role;
                if(role == "warrior") {
                    me.memory.target = name;
                }
            }
        }
        */
    }

};

module.exports = roleHealer;

